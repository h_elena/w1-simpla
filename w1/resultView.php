<?php

require_once('view/View.php');

class W1View extends View
{
	public function __construct(){
		parent::__construct();
	}
  
	public function fetch(){
		$this->design->assign('cart',		$this->cart->get_cart());
		$this->design->assign('categories', $this->categories->get_categories_tree());
		$pages = $this->pages->get_pages(array('visible'=>1));		
		$this->design->assign('pages', $pages);
		
		$content = $this->design->fetch('payment/w1/result.tpl');
		$this->design->assign('content', $content);		
    
		$wrapper = $this->design->smarty->getTemplateVars('wrapper');
		if(empty($wrapper)){
      $wrapper = 'index.tpl';
    }
		$this->body = $this->design->fetch($wrapper);
		return $this->body;
	}
	
}