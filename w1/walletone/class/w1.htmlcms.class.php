<?php
namespace WalletOne;

class W1HtmlCms extends W1Html  {
  
  /**
   * Url to pic. 
   * 
   * @var string 
   */
  public $logoUrl;

  public function __construct($lang = 'ru') {
    if (!defined('w1PathImg')) {
      define('w1PathImg', '');
    }
    
    $defaultLang = 'ru';
    if($lang != 'ru'){
      $defaultLang = 'en';
    }
    
    include_once str_replace('\\', '/', __DIR__) . '/../lang/settings.'.$defaultLang.'.php';
    include str_replace('\\', '/', __DIR__) . '/../config.php';
    
    $this->logoUrl = $config['logoUrl'];
    
    include_once('w1.helpers.php');
  }
  
  /**
	 * Output messages + errors.
	 * 
   * @return string
	 */
	public function show_messages($errors, $messages) {
    $html = '';
		if (!empty($errors)) {
			foreach ($errors as $error) {
				$html .= '<div id="message" class="error inline"><p><strong>' . $error . '</strong></p></div>';
			}
		}
    if (!empty($messages)) {
			foreach ($messages as $message) {
				$html .= '<div id="message" class="notice notice-warning inline is-dismissible"><p><strong>' . $message . '</strong></p></div>';
			}
		}
    return $html;
	}
  
  /**
	 * Output messages + errors in 1 line.
	 * 
   * @return string
	 */
	public function show_messagesOne($errors, $messages) {
    $html = '';
		if (!empty($errors)) {
			foreach ($errors as $error) {
				$html .= $error . '<br>';
			}
		}
    if (!empty($messages)) {
			foreach ($messages as $message) {
				$html .= $message . '<br>';
			}
		}
    return $html;
	}
  
  /**
   * Select creation.
   * 
   * @param string $nameLabel
   *  Output content label.
   * @param string $desc
   *  Output description.
   * @param string $nameInput
   *  Name form element select.
   * @param array $valueArray
   *  A list of data for output to select.
   * @param string $valueDefault
   *  Default value.
   * @param string $value
   *  Present value.
   * @param string $versionCms
   *  Version of CMS.
   * @return string
   */
  public function generateSelect($nameLabel, $desc = '', $nameInput, $valueArray, $valueDefault, $value, $versionCms) {
    $html = '';
    if(intval($versionCms) == 5) {
      $html .= '<tr class="tr_payment" payment="w1" style="display:none">
        <td class="td_first">' . $nameLabel . '</td>
        <td>
                          <select name="'.$nameInput.'">';
      foreach ($valueArray as $k => $v){
        $html .= '<option value="' . $k . '"' . (!empty($value) && $value == $k 
            || empty($value) && $valueDefault == $k ? ' selected' : '')
            . '>' . $v . '</option>';
      }
      $html .= '</select>
      </tr>';
    }
    else {
      $html .= '<div class="unit tr_payment" payment="w1" style="display:none">
			<div class="infofield">' . $nameLabel .
        (!empty($desc) ?
          '<i class="tooltip fa fa-question-circle" title="'.$desc.'"></i>' : ''
        ).
        '</div>'
          . '<select name="'.$nameInput.'">';
			foreach($valueArray as $k => $v){
        $html .= '<option value="' . $k . '"' . (!empty($value) && $value == $k 
            || empty($value) && $valueDefault == $k ? ' selected' : '')
            . '>' . $v . '</option>';
			}
			$html .= '
        </select>
      </div>';
    }
    
    return $html;
  }
  
  /**
   * Creating a set of checkboxes for the pay systems.
   * 
   * @param string $paymentName
   *  Name of payment - enabled|disabled.
   * @param array $paymentActive
   *  An array of selected values.
   * @param string $filename
   *  Name file with array of payment sysytems.
   * @param string $inputName
   *  Name checkbox in CMS.
   * @return string
   */
  public function getHtmlPayments($paymentName, $paymentActive, $filename, $inputName, $typeCreateNameInput = false, $nameOfId = false){
    //check the file
    if(!file_exists($filename)){
      Helpers::logging(sprintf(w1ErrorFileExist, $filename));
      return '';
    }
    
    include_once($filename);
    $list_payments = payments();
    
    //Create an html with the data of payments methods and icons
    $html = '<div class="list-paysystem">';
    foreach ($list_payments as $list) {
      $html .= '<p><strong>' . $list['name'] . '</strong></p>';
      if (!empty($list['data'])) {
        foreach ($list['data'] as $sublist) {
          if (!empty($sublist['data'])) {
            $html .= '<span><strong>' . $sublist['name'] . '</strong></span>';
            foreach ($sublist['data'] as $key => $subsublist) {
              $name = $inputName . $paymentName . '[]';
              if($typeCreateNameInput == true){
                $name = $inputName . '[' . $paymentName . '][]';
              }
              elseif($typeCreateNameInput){
                $name = $inputName . $typeCreateNameInput . '[' . $paymentName . ']';
              }
              $nameId = 'input_w1_'.$paymentName.'_'.$subsublist['id'];
              if($nameOfId){
                $nameId = $nameOfId . $subsublist['id'];
              }
              $html .= '<div>
                          <input type="checkbox" name="' . $name . '" value="' . $subsublist['id'] . '" id="'.$nameId.'" ';
              if(!empty($paymentActive) && is_array($paymentActive) && array_search($subsublist['id'], $paymentActive) !== false) {
                $html .= ' checked';
              }
              $html .=  '>';
              $html .= '<label for="'.$nameId.'">'
                  . '<img src="' . $this->logoUrl . $subsublist['id'] . '.png?type=pt&w=50&h=50'.'">'
                  . $subsublist['name'] . 
                  '</label>'
                  . '</div>';
            }
          }
          else {
            $name = $inputName . $paymentName . '[]';
            if ($typeCreateNameInput) {
              $name = $inputName . $typeCreateNameInput . '[' . $paymentName . ']';
            }
            $nameId = 'input_w1_' . $paymentName . '_' . $subsublist['id'];
            if ($nameOfId) {
              $nameId = $nameOfId . $subsublist['id'];
            }
            $html .= '<div><input type="checkbox" name="' . $name . '" id="'.$nameId.'" ';
            if(!empty($paymentActive) && is_array($paymentActive) && array_search($sublist['id'], $paymentActive) !== false){
              $html .= ' checked';
            }
            $html .= ' value="' . $sublist['id'] . '">';
            $html .= '<label for="'.$nameId.'">'
                . '<img src="' . $this->logoUrl . $sublist['id'] . '.png?type=pt&w=50&h=50'.'">'
                . $sublist['name']
                . '</label>'
                . '</div>';
          }
        }
      }
    }
    $html .= '</div>';
    return $html;
  }
  
  /**
   * Creating array of payment system with icons.
   * 
   * @param string $filename
   *  Name file with array of payment sysytems.
   * @return array
   */
  public function getArrayPayments($filename){
    //check the file
    if(!file_exists($filename)){
      Helpers::logging(sprintf(w1ErrorFileExist, $filename));
      return '';
    }
    
    include_once($filename);
    $list_payments = payments();
    
    //Create an array with the data of payments methods and icons
    $mas_payments = array();
    foreach ($list_payments as $list) {
      if (!empty($list['data'])) {
        foreach ($list['data'] as $sublist) {
          if (!empty($sublist['data'])) {
            foreach ($sublist['data'] as $subsublist) {
              $img = '<img width="30" src="'. $this->logoUrl . $subsublist['id'] . '.png?type=pt&w=50&h=50'.'">';
              $mas_payments[$subsublist['id']] = $img.' '.$subsublist['name'] . ($subsublist['name'] != $sublist['name'] ? ' (' . $sublist['name'] . ')' : '');
            }
          }
          else {
            $img = '<img width="30" src="'. $this->logoUrl . $sublist['id'] . '.png?type=pt&w=50&h=50'.'">';
            $mas_payments[$sublist['id']] = $img.' '.$sublist['name'];
          }
        }
      }
    }
    
    if(empty($mas_payments)){
      Helpers::logging(w1ErrorCreatePayments);
    }
    
    return $mas_payments;
  }
  
  /**
   * Creating array of payment system with icons for CMS PrestaShop.
   * 
   * @param string $filename
   *  Name file with array of payment sysytems.
   * @return array
   */
  public function getArrayPaymentsPresta($filename){
    //check the file
    if(!file_exists($filename)){
      Helpers::logging(sprintf(w1ErrorFileExist, $filename));
      return '';
    }
    
    include_once($filename);
    $list_payments = payments();
    
    //Create an array with the data of payments methods and icons
    $mas_payments = array();
    foreach ($list_payments as $list) {
      if (!empty($list['data'])) {
        foreach ($list['data'] as $sublist) {
          if (!empty($sublist['data'])) {
            foreach ($sublist['data'] as $subsublist) {
              $img = '<img width="30" src="'. $this->logoUrl . $subsublist['id'] . '.png?type=pt&w=50&h=50'.'">';
              $mas_payments[] = array(
								'id' => $subsublist['id'],
								'name' => $img.' '.$subsublist['name'] . ($subsublist['name'] != $sublist['name'] ? ' (' . $sublist['name'] . ')' : ''),
								'val' => $subsublist['id']
							);
            }
          }
          else {
            $img = '<img width="30" src="'. $this->logoUrl . $sublist['id'] . '.png?type=pt&w=50&h=50'.'">';
            $mas_payments[] = array(
								'id' => $sublist['id'],
								'name' => $img.' '.$sublist['name'],
								'val' => $subsublist['id']
							);
          }
        }
      }
    }
    
    if(empty($mas_payments)){
      Helpers::logging(w1ErrorCreatePayments);
    }
    
    return $mas_payments;
  }
  
  /**
   * Creating array of payment system without icons.
   * 
   * @param string $filename
   *  Name file with array of payment sysytems.
   * @return array
   */
  public function getArrayOnlyPayments($filename){
    //check the file
    if(!file_exists($filename)){
      Helpers::logging(sprintf(w1ErrorFileExist, $filename));
      return '';
    }
    
    include_once($filename);
    $list_payments = payments();
    
    //Create an array with the data of payments methods and icons
    $mas_payments = array();
    foreach ($list_payments as $list) {
      if (!empty($list['data'])) {
        foreach ($list['data'] as $sublist) {
          if (!empty($sublist['data'])) {
            foreach ($sublist['data'] as $subsublist) {
              $mas_payments[$subsublist['id']] = $subsublist['name'];
            }
          }
          else {
            $mas_payments[$sublist['id']] = $sublist['name'];
          }
        }
      }
    }
    
    if(empty($mas_payments)){
      Helpers::logging(w1ErrorCreatePayments);
    }
    
    return $mas_payments;
  }
  
  /**
   * Creating array of payment system with icons and for json array.
   * 
   * @param string $filename
   *  Name file with array of payment sysytems.
   * @return array
   */
  public function getArrayJsonPayments($filename, $ptenabled, $ptdisabled){
    //check the file
    if(!file_exists($filename)){
      Helpers::logging(sprintf(w1ErrorFileExist, $filename));
      return '';
    }
    
    include_once($filename);
    $list_payments = payments();
    
    //Create an array with the data of payments methods and icons
    $mas_payments = array();
    foreach ($list_payments as $list) {
      if (!empty($list['data'])) {
        foreach ($list['data'] as $sublist) {
          if (!empty($sublist['data'])) {
            foreach ($sublist['data'] as $subsublist) {
              $checked_enabl = false;
              if (!empty($ptenabled) && is_array($ptenabled) && array_search($subsublist['id'], $ptenabled) !== false) {
                $checked_enabl = true;
              }
              $checked_dis = false;
              if (!empty($ptdisabled) && is_array($ptdisabled) && array_search($subsublist['id'], $ptdisabled) !== false) {
                $checked_dis = true;
              }
              $mas_payments[] = array(
                'id' => $subsublist['id'],
                'name' => $subsublist['name'],
                'checked_enabl' => $checked_enabl,
                'checked_dis' => $checked_dis,
                'icon' => $this->logoUrl . $subsublist['id'] . '.png?type=pt&w=50&h=50'
              );
            }
          }
          else {
            $checked_enabl = false;
            if (!empty($ptenabled) && is_array($ptenabled) && array_search($sublist['id'], $ptenabled) !== false) {
              $checked_enabl = true;
            }
            $checked_dis = false;
            if (!empty($ptdisabled) && is_array($ptdisabled) && array_search($sublist['id'], $ptdisabled) !== false) {
              $checked_dis = true;
            }
            $mas_payments[] = array(
              'id' => $sublist['id'],
              'name' => $sublist['name'],
              'checked_enabl' => $checked_enabl,
              'checked_dis' => $checked_dis,
              'icon' => $this->logoUrl . $sublist['id'] . '.png?type=pt&w=50&h=50'
            );
          }
        }
      }
    }
    
    if(empty($mas_payments)){
      Helpers::logging(w1ErrorCreatePayments);
    }
    
    return $mas_payments;
  }
  
  /**
   * The creates picture with set of icons.
   * 
   * @param array $mas_ptenabled
   * @param array $mas_ptdisabled
   * @param srting $filename
   * @return boolean|string
   */
  public function createIcon($mas_ptenabled = array(), $mas_ptdisabled = array(), $filename, $curencyCode) {
    //check the file
    if(!file_exists($filename)){
      Helpers::logging(sprintf(w1ErrorFileExist, $filename));
      return array();
    }
    
    include_once($filename);
    $list_payments = payments();
    if(!$list_payments || !is_array($list_payments)){
      Helpers::logging(w1ErrorFileRead);
      return false;
    }
    
    $listMainPtdisabled = array();
    if(!empty($mas_ptdisabled)){
      foreach ($mas_ptdisabled as $key => $value) {
        $str = explode('/', $value);
        $str = explode('.', $str[count($str)-1]);
        $code = str_replace($curencyCode, '', $str[0]);
        if(empty($listMainPtdisabled) && !empty($code) || !in_array($code, $listMainPtdisabled) && !empty($listMainPtdisabled)){
          $listMainPtdisabled[] = $code;
        }
      }
    }
    
    $listMainPtenabled = array();
    if(!empty($mas_ptenabled)){
      foreach ($mas_ptenabled as $key => $value) {
        $str = explode('/', $value);
        $str = explode('.', $str[count($str)-1]);
        $code = str_replace($curencyCode, '', $str[0]);
        if(!empty($listMainPtenabled) && !in_array($code, $listMainPtenabled) && (empty($listMainPtdisabled) || !in_array($code, $listMainPtdisabled))){
          $listMainPtenabled[] = $code;
        }
      }
    }
    
    $listMainPayments = array();
    $masIcons = array();
    foreach ($list_payments as $list) {
      if (!empty($list['data'])) {
        foreach ($list['data'] as $sublist) {
          if (!empty($sublist['data'])) {
            foreach ($sublist['data'] as $subsublist) {
              if ((!empty($mas_ptenabled) && is_array($mas_ptenabled) && array_search($subsublist['id'], $mas_ptenabled) !== false
                && (empty($mas_ptdisabled) || array_search($subsublist['id'], $mas_ptdisabled) === false)) 
                || (empty($mas_ptenabled) && (empty($mas_ptdisabled) || (is_array($mas_ptdisabled) && array_search($subsublist['id'], $mas_ptdisabled) === false))
                && !empty($subsublist['required']) && $subsublist['required'] == 1)) {
                $code = str_replace($curencyCode, '', $subsublist['id']);
                if((empty($listMainPtenabled) && empty($listMainPtdisabled))
                    || (!empty($listMainPtenabled) && in_array($code, $listMainPtenabled) 
                        && (empty($listMainPtdisabled) || !in_array($code, $listMainPtdisabled)))
                    && !in_array($code, $listMainPayments)
                    || (empty($listMainPtenabled) && !empty($listMainPtdisabled) && !in_array($code, $listMainPtdisabled)
                        && !in_array($code, $listMainPayments))){
                  $masIcons[] = $this->logoUrl . $subsublist['id'] . '.png?type=pt&w=50&h=50';
                  $listMainPayments[] = $code;
                }
              }
            }
          }
          elseif ((!empty($mas_ptenabled) && is_array($mas_ptenabled) && array_search($sublist['id'], $mas_ptenabled) !== false
              && (empty($mas_ptdisabled) || array_search($sublist['id'], $mas_ptdisabled) === false)) 
              || (empty($mas_ptenabled) && (empty($mas_ptdisabled) || (is_array($mas_ptdisabled) && array_search($subsublist['id'], $mas_ptdisabled) === false))
                && !empty($sublist['required']) && $sublist['required'] == 1)) {
            $code = str_replace($curencyCode, '', $sublist['id']);
            if((empty($listMainPtenabled) && empty($listMainPtdisabled))
                    || (!empty($listMainPtenabled) && in_array($code, $listMainPtenabled) 
                        && (empty($listMainPtdisabled) || !in_array($code, $listMainPtdisabled)))
                    && !in_array($code, $listMainPayments)
                    || (empty($listMainPtenabled) && !empty($listMainPtdisabled) && !in_array($code, $listMainPtdisabled)
                        && !in_array($code, $listMainPayments))){
              $masIcons[] = $this->logoUrl . $sublist['id'] . '.png?type=pt&w=50&h=50';
              $listMainPayments[] = $code;
            }
          }
        }
      }
    }
    
    if(empty($masIcons)){
      Helpers::logging(w1ErrorCreateIcon);
      return false;
    }
    
    $masIcons = array_unique($masIcons);
    $picPath = $this->createPic($masIcons);
    
    return $picPath;
  }
  
  /**
   * The creates picture with set of icons.
   * 
   * @param array $mas_ptenabled
   * @param array $mas_ptdisabled
   * @param srting $filename
   * @return boolean|string
   */
  public function createIconNoEmpty($mas_ptenabled = array(), $mas_ptdisabled = array(), $filename, $curencyCode) {
    //check the file
    if(!file_exists($filename)){
      Helpers::logging(sprintf(w1ErrorFileExist, $filename));
      return array();
    }
    
    include_once($filename);
    $list_payments = payments();
    if(!$list_payments || !is_array($list_payments)){
      Helpers::logging(w1ErrorFileRead);
      return false;
    }
    
    $listMainPtdisabled = array();
    if(!empty($mas_ptdisabled)){
      $mas_ptdisabled = array_diff($mas_ptdisabled, array(null, false, 0));
      foreach ($mas_ptdisabled as $key => $value) {
        $str = explode('/', $key);
        $str = explode('.', $str[count($str)-1]);
        $code = str_replace($curencyCode, '', $str[0]);
        if(!in_array($code, $listMainPtdisabled)){
          $listMainPtdisabled[] = $code;
        }
      }
    }
    
    $listMainPtenabled = array();
    if(!empty($mas_ptenabled)){
      $mas_ptenabled = array_diff($mas_ptenabled, array(null, false, 0));
      foreach ($mas_ptenabled as $key => $value) {
        $str = explode('/', $key);
        $str = explode('.', $str[count($str)-1]);
        $code = str_replace($curencyCode, '', $str[0]);
        if(!in_array($code, $listMainPtenabled) && (empty($listMainPtdisabled) || !in_array($code, $listMainPtdisabled))){
          $listMainPtenabled[] = $code;
        }
      }
    }
    
    $listMainPayments = array();
    $masIcons = array();
    foreach ($list_payments as $list) {
      if (!empty($list['data'])) {
        foreach ($list['data'] as $sublist) {
          if (!empty($sublist['data'])) {
            foreach ($sublist['data'] as $subsublist) {
              if ((!empty($mas_ptenabled[$subsublist['id']]) && $mas_ptenabled[$subsublist['id']] == $subsublist['id']
                && empty($mas_ptdisabled[$subsublist['id']])) 
                || (empty($mas_ptenabled[$subsublist['id']]) && empty($mas_ptdisabled[$subsublist['id']])
                && !empty($subsublist['required']) && $subsublist['required'] == 1)) {
                $code = str_replace($curencyCode, '', $subsublist['id']);
                if((empty($listMainPtenabled) && empty($listMainPtdisabled))
                    || (!empty($listMainPtenabled) && in_array($code, $listMainPtenabled) 
                        && (empty($listMainPtdisabled) || !in_array($code, $listMainPtdisabled)))
                    && !in_array($code, $listMainPayments)
                    || (empty($listMainPtenabled) && !empty($listMainPtdisabled) && !in_array($code, $listMainPtdisabled)
                        && !in_array($code, $listMainPayments))){
                  
                  $masIcons[] = $this->logoUrl . $subsublist['id'] . '.png?type=pt&w=50&h=50';
                  $listMainPayments[] = $code;
                }
              }
            }
          }
          elseif ((!empty($mas_ptenabled[$sublist['id']]) && $mas_ptenabled[$sublist['id']] == $sublist['id']
                && empty($mas_ptdisabled[$sublist['id']])) 
                || (empty($mas_ptenabled[$sublist['id']]) && empty($mas_ptdisabled[$sublist['id']])
                && !empty($sublist['required']) && $sublist['required'] == 1)) {
            $code = str_replace($curencyCode, '', $sublist['id']);
            if((empty($listMainPtenabled) && empty($listMainPtdisabled))
                    || (!empty($listMainPtenabled) && in_array($code, $listMainPtenabled) 
                        && (empty($listMainPtdisabled) || !in_array($code, $listMainPtdisabled)))
                    && !in_array($code, $listMainPayments)
                    || (empty($listMainPtenabled) && !empty($listMainPtdisabled) && !in_array($code, $listMainPtdisabled)
                        && !in_array($code, $listMainPayments))){
              $masIcons[] = $this->logoUrl . $sublist['id'] . '.png?type=pt&w=50&h=50';
              $listMainPayments[] = $code;
            }
          }
        }
      }
    }
    
    if(empty($masIcons)){
      Helpers::logging(w1ErrorCreateIcon);
      return false;
    }
    
    $masIcons = array_unique($masIcons);
    $picPath = $this->createPic($masIcons);
    
    return $picPath;
  }
  
  /**
   * Create and save picture.
   * 
   * @param array $masPic
   * @return boolean|string
   */
  private function createPic($masPic){
    $allowed_types = array('jpg', 'jpeg', 'png');
    $nw = 50 * count($masPic);
    $nh = 50;
    //creation of a new canvas.
    $img = imagecreatetruecolor($nw, $nh);
    $col2 = ImageColorAllocate($img, 255, 255, 255);
    ImageFilledRectangle($img, 0, 0, $nw, $nh, $col2);
    imagecolortransparent($img, imagecolorallocate($img, 255, 255, 255));
    $i = 0;

    foreach ($masPic as &$file) {
      $str = explode('/', $file);
      $str[count($str) - 1] = stristr($str[count($str) - 1], '?', true);
      $content = file_get_contents($file);
      file_put_contents(str_replace('\\', '/', __DIR__) . '/../tmp/' . $str[count($str) - 1], $content);
      
      $file = str_replace('\\', '/', __DIR__) . '/../tmp/' . $str[count($str) - 1];
      $fileParts = explode('.', $file);
      $ext = strtolower(array_pop($fileParts));

      if (in_array($ext, $allowed_types)) {
        if ($ext == 'jpeg') {
          $ext = 'jpg';
        }
        if ($ext == 'jpg') {
          if(!$pic = imagecreatefromjpeg($file)){
            Helpers::logging(w1ErrorCreateImage);
            return false;
          }
        }
        elseif ($ext == 'png') {
          if(!$pic = imagecreatefrompng($file)){
            Helpers::logging(w1ErrorCreateImage);
            return false;
          }
        }
        //put an icon on the canvas
        imagecopy($img, $pic, $i, 0, 0, 0, 50, 50);
        $i += 50;
      }
    }
    //save the canvas
    $fileNew = str_replace('\\', '/', __DIR__) . '/../img/new_icon.' . $ext;
    if(file_exists($fileNew)){
      unlink($fileNew);
    }
    if ($ext == 'jpg') {
      if (!imagejpeg($img, $fileNew)) {
        Helpers::logging(w1ErrorCopyImage);
        return false;
      }
    }
    elseif ($ext == 'png') {
      if (!imagepng($img, $fileNew)) {
        Helpers::logging(w1ErrorCopyImage);
        return false;
      }
    }
    imagedestroy($img);
    imagedestroy($pic);
    $fileNew = 'new_icon.' . $ext;
    if(file_exists(str_replace('\\', '/', __DIR__) . '/../tmp/')){
      $scanned_directory = array_diff(scandir(str_replace('\\', '/', __DIR__) . '/../tmp/'), array('..', '.'));
      foreach ($scanned_directory as $value) {
        unlink(str_replace('\\', '/', __DIR__) . '/../tmp/'.$value);
      }
    }
    return $fileNew;
  }

  

  /**
   * Create for for payment.
   * 
   * @param type $fields
   *  Array of all data.
   * @param type $paymentUrl
   *  Redirect url of payment system.
   * @param type $canelUrl
   * @return string
   */
  public function createForm($fields, $paymentUrl, $canelUrl = '', $redirect = false){
    $html = '<form action="' . $paymentUrl . '" method="POST" id="formPayment">';
    foreach ($fields as $key => $value) {
      if(is_array($value)){
        foreach ($value as $v) {
          $html .= '<input type="hidden" value="' . $v . '" name="' . $key . '">';
        }
      }
      else{
        $html .= '<input type="hidden" value="' . $value . '" name="' . $key . '">';
      }
    }
    $html .= '<input type="submit" class="button alt" value="' . w1OrderSubmitShort . '" /> ';
    if(!empty($canelUrl)){
      $html .= '<a class="button cancel" href="' . $canelUrl . '">' . w1OrderSubmitBack . '</a>' . "\n";
    }
    $html .= '</form>';
    if($redirect){
      $html .= '<script>document.getElementById("formPayment").submit();</script>';
    }
    
    return $html;
  }
  
}
