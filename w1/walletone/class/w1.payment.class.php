<?php
namespace WalletOne;

/**
 * Abstract class for data of payment.
 * 
 */
abstract class W1Payment {

  /**
   * Link to go to the payment system.
   * 
   * @var string
   */
  public $paymentUrl;
  
  /**
   * The number of CMS in the list.
   * 
   * @var int 
   */
  public $numCms;
  
  /**
   * Name of site.
   * 
   * @var string
   */
  public $siteName;
  
  /**
   * Link in the case of successful payment.
   * 
   * @var string
   */
  public $successUrl;
  
  /**
   * Link in the case does not of successful payment.
   * 
   * @var string
   */
  public $failUrl;

  /**
   * The set of required invoce fields.
   * 
   * @var array
   */
  public $requiredFields = array();
  
  /**
   * The array for check to the regular expressions.
   * 
   * @var array
   */
  public $fieldsPreg = array();
  
  /**
   * The array with all errors;
   * 
   * @var array 
   */
  public $errors = array();
  
  /**
   * To check the required fields.
   * 
   * @return boolean
   */
  abstract public function required();
  
  /**
   * Checks for required fields and cheks validation these fields.
   * 
   * @param array $param
   * 
   * @return boolean
   */
  abstract public function validation();

  /**
   * 
   * 
   * @return string
   */
  abstract public function createFormArray($settings, $invoce);
  
}
