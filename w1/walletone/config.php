<?php

$config = array(
  'paymentUrl' => 'https://wl.walletone.com/checkout/checkout/Index',
  'settings' => array(
    'requiredFields' => array(
      0 => 'merchantId',
      1 => 'signature',
      2 => 'signatureMethod',
      3 => 'currencyId',
      4 => 'currencyDefault'
    ),
    'fieldsPreg' => array(
      'merchantId' => '/[0-9]{10,15}$/ui',
      'signature' => '/[a-z0-9]{3,250}$/ui',
      'signatureMethod' => '/(md5|sha1)$/ui',
      'currencyId' => '/[0-9]{3,4}$/ui',
      'currencyDefault' => '/[a-z0-9]{1,5}$/ui',
      'orderStatusSuccess' => '/[a-z0-9\s\-_.,]{1,250}$/ui',
      'orderStatusWaiting' => '/[a-z0-9\s\-_.,]{1,250}$/ui',
      'orderStatusFail' => '/[a-z0-9\s\-_.,]{1,250}$/ui',
      'paymentSystemEnabled' => '/[a-z0-9]{3,250}$/ui',
      'paymentSystemDisabled' => '/[a-z0-9]{3,250}$/ui',
    ),
    'fieldsName' => array(
      'merchantId' => w1SettingsMerchant,
      'signatureMethod' => w1SettingsSignatureMethod,
      'signature' => w1SettingsSignature,
      'currencyId' => w1SettingsCurrency,
      'orderStatusSuccess' => w1SettingsOrderStatusSuccess,
      'orderStatusWaiting' => w1SettingsOrderStatusWaiting,
      'orderStatusFail' => w1SettingsOrderStatusFail,
      'currencyDefault' => w1SettingsCurrencyDefault,
      'paymentSystemEnabled' => w1SettingsPtenabled,
      'paymentSystemDisabled' => w1SettingsPtdisabled,
    )
  ),
  'invoce' => array(
    'requiredFields' => array(
      0 => 'orderId',
      1 => 'summ',
      2 => 'currencyId',
    ),
    'fieldsPreg' => array(
      'orderId' => '/[0-9a-z_\-]{1,550}$/ui',
      'summ' => '/[0-9\s\,\.]{1,250}$/ui',
      'currencyId' => '/[0-9]{3,4}$/ui',
      'firstNameBuyer' => '/^[a-zа-яё\s\-]{1,250}$/ui',
      'lastNameBuyer' => '/^[a-zа-яё\s\-]{1,250}$/ui',
      'emailBuyer' => '/^[a-z0-9а-яё\s\-_\.@\+]{1,500}$/ui',
    ),
    'fieldsName' => array(
      'orderId' => w1orderId,
      'summ' => w1summ,
      'currencyId' => w1currencyId,
      'firstNameBuyer' => w1firstNameBuyer,
      'lastNameBuyer' => w1lastNameBuyer,
      'emailBuyer' => w1emailBuyer,
    )
  ),
  'payment' => array(
    'cms' => 0,
    'cms_wordpress' => 10,
    'cms_opencart' => 15,
    'cms_prestashop' => 20,
    'cms_simpla' => 24,
    'cms_diafan' => 25,
    'cms_drupalCommerce' => 26,
    'cms_shopkeeper' => 28,
    'cms_readyscript' => 31,
    'cms_minishop' => 33,
    'cms_ecwid' => 34,
    'requiredFields' => array(
      0 => 'siteName',
      1 => 'successUrl',
      2 => 'failUrl',
    ),
    'fieldsPreg' => array(
      'siteName' => '/[a-zа-яё0-9\:\/.\-_]{3,3000}$/ui',
      'successUrl' => '/[a-zа-яё0-9\:\/.\-_\?\&\=]{3,3000}$/ui',
      'failUrl' => '/[a-zа-яё0-9\:\/.\-_\?\&\=]{3,3000}$/ui',
    ),
    'fieldsName' => array(
      'siteName' => w1siteName,
      'successUrl' => w1successUrl,
      'failUrl' => w1failUrl,
    ),
  ),
  'result' => array(
    'requiredFields' => array(
      0 => 'orderPaymentId',
      1 => 'orderState',
      2 => 'orderId',
      3 => 'signature',
      4 => 'summ'
    ),
    'fieldsPreg' => array(
      'orderPaymentId' => '/[0-9]{1,15}$/ui',
      'orderState' => '/[a-z]{1,10}$/ui',
      'paymentType' => '/[a-z0-9\s_\-,.]{1,250}$/ui',
      'orderId' => '/[0-9a-z\s\-_.а-яё]{1,550}$/ui',
      'summ' => '/[0-9\s\,\.]{1,250}$/',
      'signature' => '/[a-z0-9\._\s\-\+\=\/\\\]{3,250}$/ui'
    ),
    'fieldsName' => array(
      'orderPaymentId' => w1orderPaymentId,
      'orderState' => w1orderState,
      'orderId' => w1orderId,
      'paymentType' => w1paymentType,
      'signature' => w1signature,
      'summ' => w1summ
    )
  ),
  'currencyCode' => array(
    643 => 'RUB',
    710 => 'ZAR',
    840 => 'USD',
    978 => 'EUR',
    980 => 'UAH',
    398 => 'KZT',
    974 => 'BYR',
    972 => 'TJS',
    985 => 'PLN',
    981 => 'GEL'
  ),
  'currencyName' => array(
    0 => w1SettingsCurrency_0,
    643 => w1SettingsCurrency_643,
    710 => w1SettingsCurrency_710,
    840 => w1SettingsCurrency_840,
    978 => w1SettingsCurrency_978,
    980 => w1SettingsCurrency_980,
    398 => w1SettingsCurrency_398,
    974 => w1SettingsCurrency_974,
    972 => w1SettingsCurrency_972,
    985 => w1SettingsCurrency_985,
    981 => w1SettingsCurrency_981
  ),
  'currencyDefault' => 'no',
  'signatureMethodDefault' => 'MD5',
  'signatureMethod' => array(
    '0' => w1SettingsSignatureMethod_0,
    'md5' => 'MD5',
    'sha1' => 'SHA1'
  ),
  'currencyPresta' => array(
    array(
      'id' => 0,
      'name' => w1SettingsCurrency_0
    ),
    array(
      'id' => 643,
      'name' => w1SettingsCurrency_643
    ),
    array(
      'id' => 710,
      'name' => w1SettingsCurrency_710
    ),
    array(
      'id' => 840,
      'name' => w1SettingsCurrency_840
    ),
    array(
      'id' => 978,
      'name' => w1SettingsCurrency_978
    ),
    array(
      'id' => 980,
      'name' => w1SettingsCurrency_980
    ),
    array(
      'id' => 398,
      'name' => w1SettingsCurrency_398
    ),
    array(
      'id' => 974,
      'name' => w1SettingsCurrency_974                                                                  
    ),
    array(
      'id' => 972,
      'name' => w1SettingsCurrency_972
    ),
    array(
      'id' => 985,
      'name' => w1SettingsCurrency_985
    ),
    array(
      'id' => 981,
      'name' => w1SettingsCurrency_981
    ),
  ),
  'signatureMethodPresta' => array(
    array(
      'id' => 0,
      'name' => w1SettingsSignatureMethod_0
    ),
    array(
      'id' => 'md5',
      'name' => 'MD5'
    ),
    array(
      'id' => 'sha1',
      'name' => 'SHA1'
    ),
  ),
  'logoUrl' => 'https://www.walletone.com/logo/provider/'
);

